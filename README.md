# IrcEx

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `irc_ex` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:irc_ex, "~> 0.1.0"}]
    end
    ```

  2. Ensure `irc_ex` is started before your application:

    ```elixir
    def application do
      [applications: [:irc_ex]]
    end
    ```

