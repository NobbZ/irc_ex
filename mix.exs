defmodule IrcEx.Mixfile do
  use Mix.Project

  @prod_like_envs [:prod, :dialyxir]

  def project do
    [
      app: :irc_ex,
      version: "0.1.0",
      elixir: "~> 1.3",
      build_embedded: Mix.env in @prod_like_envs,
      start_permanent: Mix.env in @prod_like_envs,
      deps: deps,
      dialyzer: dialyzer,
      aliases: aliases,
    ]
  end

  def application do
    [applications: [:logger, :combine]]
  end

  defp deps do
    [
      {:combine, "~> 0.9"},

      {:credo, "~> 0.5", only: :dev},
      {:inch_ex, "~> 0.5", only: :dev},

      {:dialyxir, "~> 0.4", only: :dialyxir},
    ]
  end

  defp dialyzer do
    core_flags = [
      flags: ["-Wunmatched_returns", "-Wunderspecs"]
    ]
    ci_flags = [
      plt_core_path: ".cache/",
      plt_file: ".cache/irc_ex.plt",
    ]

    case System.get_env("CI") do
      nil -> core_flags
      _   -> core_flags ++ ci_flags
    end
  end

  defp aliases do
    [
      check: ["dialyzer", "credo", "inch"],
      dialyzer: [&mk_cache/1, "dialyzer"],
    ]
  end

  def mk_cache(_) do
    if System.get_env("CI"), do: File.mkdir_p(".cache/")
  end
end
