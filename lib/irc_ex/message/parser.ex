defmodule IrcEx.Message.Parser do
  use Combine

  def parse(input) do
    Combine.parse(input, message)
  end

  def message(prev \\ nil) do
    prev
    |> option(prefix)
    |> command
    #|> params
    #|> crlf
  end

  def prefix, do: ignore(char(?:)) |> map(take_while(fn ?\s  -> false; _ -> true end), fn pfx -> {:prefix, to_string(pfx)} end) |> ignore(space)

  def command(prev \\ nil), do: prev |> either(map(many1(letter), &Enum.join/1), map(times(digit, 3), fn l -> l |> Enum.map(&to_string/1) |> Enum.join end))

  # def params(prev \\ nil), do: prev |> times(option(middle), 14)

  def crlf(prev \\ nil), do: prev |> string("\r\n")

  # def middle(prev \\ nil), do: nospcrlfcl |> many(either(char(?:), nospcrlfcl))
end
