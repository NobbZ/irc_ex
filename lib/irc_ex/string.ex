defmodule IrcEx.String do
  @moduledoc """
  This module can be used exactly like `String` with the only difference that its `upcase/1` and `downcase/1` are
  conforming the RFCs.
  """

  @spec capitalize(String.t) :: String.t
  def capitalize(string)
  def capitalize(<<"[", remainder::binary>>), do: "{" <> remainder
  def capitalize(<<"]", remainder::binary>>), do: "}" <> remainder
  def capitalize(<<"\\", remainder::binary>>), do: "|" <> remainder
  def capitalize(<<"~", remainder::binary>>), do: "^" <> remainder
  def capitalize(string), do: String.capitalize string

  @spec downcase(String.t) :: String.t
  def downcase(string) do
    string
    |> String.downcase
    |> String.replace("{", "[")
    |> String.replace("}", "]")
    |> String.replace("|", "\\")
    |> String.replace("^", "~")
  end

  @spec upcase(String.t) :: String.t
  def upcase(string) do
    string
    |> String.upcase
    |> String.replace("[", "{")
    |> String.replace("]", "}")
    |> String.replace("\\", "|")
    |> String.replace("~", "^")
  end
end
