defmodule IrcEx.StringTest do
  use ExUnit.Case

  alias IrcEx.String

  @uppercase ~w({ } | ^)
  @lowercase ~w([ ] \\ ~)

  describe "capitalize" do
    @lowercase |> Enum.zip(@uppercase) |> Enum.map(fn {l, u} -> {l <> "foo", u <> "foo"} end) |> Enum.each(fn {from, to} ->
      test "#{from} to #{to}", do: assert unquote(to) = String.capitalize(unquote(from))
    end)
  end

  describe "downcase" do
    @uppercase |> Enum.zip(@lowercase) |> Enum.each(fn {from, to} ->
      test "#{from} to #{to}", do: assert unquote(to) = String.downcase(unquote(from))
    end)
  end

  describe "upcase" do
    @lowercase |> Enum.zip(@uppercase) |> Enum.each(fn {from, to} ->
      test "#{from} to #{to}", do: assert unquote(to) = String.upcase(unquote(from))
    end)
  end

end
