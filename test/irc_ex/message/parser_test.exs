defmodule IrcEx.Message.ParserTest do
  use ExUnit.Case

  alias IrcEx.Message.Parser

  @tag :skip
  test "complete" do
    foo = Combine.parse(":Example!example@example.com JOIN :#Kauze\r\n", Parser.message)
    assert [] = foo
  end

  test "prefix" do
    foo = Combine.parse(":Example!example@example.com ", Parser.prefix)
    assert [prefix: "Example!example@example.com"] = foo
  end

  test "text command" do
    foo = Combine.parse("JOIN", Parser.command)
    assert ["JOIN"] = foo
  end

  test "number command" do
    foo = Combine.parse("123", Parser.command)
    assert ["123"] = foo
  end

  @tag :skip
  test "params" do
    foo = Combine.parse(":#kauze", Parser.params)
    assert [] = foo
  end
end
